package br.ucsal.poo.avaliacaoAntiga.tui;

import java.util.List;

import br.ucsal.poo.avaliacaoAntiga.domain.Contrato;
import br.ucsal.poo.avaliacaoAntiga.domain.Veiculo;
import br.ucsal.poo.avaliacaoAntiga.enums.TipoVeiculo;

public class ContratoTUI {

	public static void main(String[] args) {
		Contrato contrato1 = new Contrato("Danilo Leone", "Salvador, BA");
		Contrato contrato2 = new Contrato("Stephanie", "Lauro de Freitas, BA");
		
		Veiculo veiculo1 = new Veiculo("pik-1420", 2017, TipoVeiculo.INTERMEDIARIO);
		Veiculo veiculo2 = new Veiculo("nzn-8666", 2011, TipoVeiculo.BASICO);
		Veiculo veiculo3 = new Veiculo("pzo-3152", 2019, TipoVeiculo.LUXO);
		
		contrato1.adicionarVeiculo(veiculo1);
		contrato1.adicionarVeiculo(veiculo2);
		contrato2.adicionarVeiculo(veiculo3);
		
		List<Veiculo> veiculosContrato1 = contrato1.consultarVeiculos();
		
		System.out.println("Primeira listagem de Contrato 1:");
		for (Veiculo veiculo : veiculosContrato1) {
			System.out.println(veiculo.toString());
		}
		
		contrato1.removerVeiculo("nzn-8666");
		
		System.out.println("Segunda listagem de Contrato 1:");
		for (Veiculo veiculo : veiculosContrato1) {
			System.out.println(veiculo.toString());
		}
	}

}
