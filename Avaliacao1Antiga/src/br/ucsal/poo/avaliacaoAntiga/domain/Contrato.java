package br.ucsal.poo.avaliacaoAntiga.domain;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.poo.avaliacaoAntiga.enums.TipoVeiculo;

public class Contrato {
	private static Integer numeroTotalContas = 0;

	private Integer numeroDoContrato;
	private String nomeDoCliente;
	private String enderecoDoCliente;
	private List<Veiculo> veiculos = new ArrayList<>();
	private Double valorTotalContrato = 0d;

	public Contrato(String nomeDoCliente, String enderecoDoCliente) {
		numeroTotalContas += 1;

		this.numeroDoContrato = numeroTotalContas;
		this.nomeDoCliente = nomeDoCliente;
		this.enderecoDoCliente = enderecoDoCliente;
	}

	public Integer getNumeroDoContrato() {
		return numeroDoContrato;
	}

	public Double getValorTotalContrato() {
		return valorTotalContrato;
	}

	public String getNomeDoCliente() {
		return nomeDoCliente;
	}

	public String getEnderecoDoCliente() {
		return enderecoDoCliente;
	}

	public Boolean adicionarVeiculo(Veiculo veiculo) {
		for (Veiculo e : veiculos) {
			if (e.getPlaca().equals(veiculo.getPlaca())) {
				return false;
			}
		}

		veiculos.add(veiculo);

		TipoVeiculo tipo = veiculo.getTipo();
		if (tipo.getValor() == 1) {
			valorTotalContrato += 100.45d;
		} else if (tipo.getValor() == 2) {
			valorTotalContrato += 130.10d;
		} else if (tipo.getValor() == 3) {
			valorTotalContrato += 156.00d;
		}

		return true;
	}

	public Boolean removerVeiculo(String placa) {
		for (int idx = 0; idx < veiculos.size(); idx++) {
			Veiculo veiculo = veiculos.get(idx);
			if (veiculo.getPlaca().equals(placa)) {

				TipoVeiculo tipo = veiculo.getTipo();
				if (tipo.getValor() == 1) {
					valorTotalContrato -= 100.45d;
				} else if (tipo.getValor() == 2) {
					valorTotalContrato -= 130.10d;
				} else if (tipo.getValor() == 3) {
					valorTotalContrato -= 156.00d;
				}

				veiculos.remove(idx);
				return true;
			}
		}
		return false;
	}

	public List<Veiculo> consultarVeiculos() {
		return veiculos;
	}

	public List<Veiculo> consultarVeiculos(TipoVeiculo tipo) {
		List<Veiculo> listaFiltrada = new ArrayList<>();
		
		for (Veiculo e : veiculos) {
			if (e.getTipo().getValor() == tipo.getValor()) {
				listaFiltrada.add(e);
			}
		}
		
		return listaFiltrada;
	}
	
	public Veiculo consultarVeiculo(String placa) {
		for (Veiculo veiculo : veiculos) {
			if (veiculo.getPlaca() == placa) {
				return veiculo;
			}
		}
		
		return null;
	}
}
