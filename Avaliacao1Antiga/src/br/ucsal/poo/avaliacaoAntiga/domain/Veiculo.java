package br.ucsal.poo.avaliacaoAntiga.domain;

import br.ucsal.poo.avaliacaoAntiga.enums.TipoVeiculo;

public class Veiculo {
	private String placa;
	private Integer anoDeFabricacao;
	private TipoVeiculo tipo;
	
	public Veiculo(String placa, Integer anoDeFabricacao, TipoVeiculo tipo) {
		this.placa = placa;
		this.anoDeFabricacao = anoDeFabricacao;
		this.tipo = tipo;
	}

	public TipoVeiculo getTipo() {
		return tipo;
	}

	@Override
	public String toString() {
		return "Veiculo [placa=" + placa + ", anoDeFabricacao=" + anoDeFabricacao + ", tipo=" + tipo + "]";
	}

	public void setTipo(TipoVeiculo tipo) {
		this.tipo = tipo;
	}

	public String getPlaca() {
		return placa;
	}

	public Integer getAnoDeFabricacao() {
		return anoDeFabricacao;
	}
}
