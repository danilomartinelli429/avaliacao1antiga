package br.ucsal.poo.avaliacaoAntiga.enums;

public enum TipoVeiculo {
	BASICO(1), INTERMEDIARIO(2), LUXO(3);
	
	private final int valor;
	
	TipoVeiculo(int valor) {
		this.valor = valor;
	}
	
	public int getValor() {
		return this.valor;
	}
}
